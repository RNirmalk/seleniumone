package loginmethod;

import org.testng.annotations.BeforeMethod;

import leafTaps.testcases.practice.SuperImplementationClass;

public class browserloginapp extends SuperImplementationClass{

	@BeforeMethod
	public void login() {
		settings();
		driver.get("http://leaftaps.com/opentaps/");
		takeSnap();
		driver.findElementById("username").sendKeys("DemoSalesManager");	
		takeSnap();

		driver.findElementById("password").sendKeys("crmsfa");
		takeSnap();

		driver.findElementByXPath("//input[@class='decorativeSubmit']").click();
		takeSnap();
		driver.findElementByLinkText("CRM/SFA").click();
		takeSnap();
	}
	
	
}
