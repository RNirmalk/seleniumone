package leafTaps.testcases.practice;


import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import leafTaps.testcases.practice.SuperImplementationClass;
import loginmethod.browserloginapp;
import leafTaps.testcases.practice.SuperImplementationClass;

import leafTaps.testcases.practice.SuperImplementationClass;

import leafTaps.testcases.practice.SuperImplementationClass;


public class CreateLead extends browserloginapp{
	

	
	
  @Test(invocationCount=2,invocationTimeOut=50000)
 public void createLead() {
	  
   
		driver.findElementByXPath("//a[@href='/crmsfa/control/leadsMain']").click();
		takeSnap();
		driver.findElementByXPath("//a[@href='/crmsfa/control/createLeadForm']").click();
		takeSnap();
		driver.findElementById("createLeadForm_companyName").sendKeys("TestCompanyName2");
		takeSnap();
		driver.findElementById("createLeadForm_firstName").sendKeys("TestFirstName2");
		takeSnap();
		driver.findElementById("createLeadForm_lastName").sendKeys("TestLastName2");
		takeSnap();
		Select selSource = new Select(driver.findElementById("createLeadForm_dataSourceId"));
		selSource.selectByVisibleText("Conference");
		driver.findElementById("createLeadForm_primaryEmail").sendKeys("TestCreateLead@gmail.com");
		takeSnap();
		driver.findElementByXPath("//input[@value='Create Lead']").click();
		takeSnap();
		String  ViewLeadExpected = "TestFirstName2";
		System.out.println(ViewLeadExpected);
		String ViewLeadActual = driver.findElementById("viewLead_firstName_sp").getText();
		System.out.println(ViewLeadActual);
		if (ViewLeadExpected.equals(ViewLeadActual)){
		System.out.println("CreateLead is verified");
		}
		driver.close();
}
	
	}




